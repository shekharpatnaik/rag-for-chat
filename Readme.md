# RAG for Chat

This is a proof of concept repository to test out how we can do RAG for chat.

```
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
python ./src/xray.py create --path="<path>" --store="k8sagent"
python ./src/xray.py search "How do I call the GitLab API from the agent?" --store="k8sagent" --debug
```