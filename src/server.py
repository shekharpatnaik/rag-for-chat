import uvicorn

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import List
from embeddings import gen_embeddings

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class EmbeddingsInput(BaseModel):
    text: str

class EmbeddingsOutput(BaseModel):
    embeddings: List[float]

@app.post("/embeddings")
def embeddings(input: EmbeddingsInput) -> EmbeddingsOutput:
    embeddings = gen_embeddings(input.text)
    return EmbeddingsOutput(embeddings=embeddings)

def run():
    uvicorn.run("server:app", host="0.0.0.0", port=9191, reload=True)
