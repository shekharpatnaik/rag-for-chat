import db
from termcolor import colored
from anthropic import Anthropic, HUMAN_PROMPT, AI_PROMPT
from embeddings import gen_embeddings

SEARCH_PROMPT = '''
Based on the following data could you please answer this question?
'''


def search_by_question(query, store_path, debug):
    client = Anthropic()

    embedding = gen_embeddings(query)

    # query_embedding = response['data'][0]['embedding']
    results = db.search(embedding, store_path, debug)
    
    prompt = f"{SEARCH_PROMPT}\nQuestion:{query}\n\nData:\n"
    for result in results:
        contents = result.payload['contents']
        prompt += f"#####\nContents:{contents}" 
    
    # print("Making request to openai")
    response = client.completions.create(
        prompt=f"{HUMAN_PROMPT} {prompt} {AI_PROMPT}",
        temperature=0.1,
        model="claude-2.1",
        max_tokens_to_sample=300
    )

    print(colored("ANSWER TO YOUR QUESTION:", "blue"))
    print(response.completion)