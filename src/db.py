from qdrant_client import QdrantClient
from qdrant_client.http import models
from termcolor import colored

def store(chunks, store_path):
    client = QdrantClient(path=store_path)
    client.recreate_collection(
        collection_name="code_base",
        vectors_config=models.VectorParams(
            size=256,  # Vector size is defined by used model
            distance=models.Distance.COSINE,
        ),
    )

    chunks = list(filter(lambda chunk: 'embedding' in chunk, chunks))

    client.upload_records(
        collection_name="code_base",
        records=[
            models.Record(
                id=idx, vector=chunk['embedding'], payload=chunk
            )
            for idx, chunk in enumerate(chunks)
        ],
    )

def search(question_embedding, store_path, debug):
    client = QdrantClient(path=store_path)

    search_result = client.search(
        collection_name="code_base",
        search_params=models.SearchParams(hnsw_ef=128, exact=False),
        query_vector=question_embedding,
        limit=10,
    )

    if debug:
        print(colored("#####INPUTS FROM VECTOR STORE####:", "yellow"))
        for result in search_result:
            filename = result.payload['filename']
            contents = result.payload['contents']
            score = result.score
            print(f'Filename: {filename}')
            print(f'Contents: {contents}')
            print(f'Score: {score}')
            print('------------------------')

    return search_result