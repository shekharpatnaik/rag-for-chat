from os import walk, path
from tree_sitter import Language, Parser
from json import dumps
from embeddings import gen_embeddings
from datetime import datetime

from db import store

IGNORE_LIST = ['venv', '.git', 'vendor']
GO_LANGUAGE = Language('build/my-languages.so', 'go')
JAVASCRIPT_LANGUAGE = Language('build/my-languages.so', 'javascript')
PY_LANGUAGE = Language('build/my-languages.so', 'python')

def find_symbols(node, filename, source, language):
  symbols = ['class_declaration', 'method_declaration', 'function_declaration', 'type_declaration', 'function_definition', 'class_definition']
  elements = []

  if node.type in symbols:
    elements.append({
      "filename": filename,
      "language": language,
      "type": node.type,
      "contents": source[node.start_byte:node.end_byte].decode('utf-8')
    })

  if node.children != None:
    for child in node.children:
      child_chunks = find_symbols(child, filename, source, language)
      elements.extend(child_chunks)

  return elements

def create_chunks(root_path, store_path, ignore_tests):
  print(f"{datetime.now()} Finding code blocks....")
  chunks = []
  for (dirpath, dirnames, filenames) in walk(root_path):
    dirnames[:] = [d for d in dirnames if d not in IGNORE_LIST]
    for filename in filenames:
      full_path = path.join(dirpath, filename)

      if ignore_tests and 'test' in full_path.lower():
        continue
      _, extension = path.splitext(filename)
      if extension == ".py" or extension == ".js" or extension == ".go":
        parser = Parser()
        if extension == ".py":
          parser.set_language(PY_LANGUAGE)
        elif extension == ".js":
          parser.set_language(JAVASCRIPT_LANGUAGE)
        elif extension == ".go":
          parser.set_language(GO_LANGUAGE)

        file_handle = open(full_path)
        file_contents = file_handle.read()
        source = bytes(file_contents, "utf8")
        tree = parser.parse(source)
        symbols = find_symbols(tree.root_node, full_path, source, extension)
        chunks.extend(symbols)
  
  print(f'{datetime.now()} Generating embeddings....')
  chunks_with_embeddings = []
  for chunk in chunks:
    embedding = gen_embeddings(chunk["contents"])
    chunk["embedding"] = embedding
    chunks_with_embeddings.append(chunk)

  print(f"{datetime.now()} Storing embeddings in qdrant....")
  store(chunks_with_embeddings, store_path)

  print(f"{datetime.now()} Storing embeddings in file....")
  with open(f'./{store_path}.json', 'w') as fp:
    fp.write(dumps(chunks_with_embeddings))
    
  print(f"{datetime.now()} Done")
