from transformers import AutoModel, AutoTokenizer

checkpoint = "Salesforce/codet5p-110m-embedding"
device = "mps"

tokenizer = AutoTokenizer.from_pretrained(checkpoint, trust_remote_code=True)
model = AutoModel.from_pretrained(checkpoint, trust_remote_code=True).to(device)

def gen_embeddings(code: str):
    inputs = tokenizer.encode(code, return_tensors="pt", truncation=True).to(device)
    embedding = model(inputs)[0]
    return embedding.detach().cpu().numpy().tolist()
    