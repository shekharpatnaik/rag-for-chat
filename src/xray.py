import fire
from create import create_chunks
from search import search_by_question
from server import run

def create(path=".", store="./mydb", ignore_tests=True):
    return create_chunks(path, store, ignore_tests)

def search(query, store="./mydb", debug=False):
    search_by_question(query, store, debug)

def server():
    run()

if __name__ == '__main__':
    fire.Fire({
        "create": create,
        "search": search,
        "server": server
    })